// Глобальные интерфейсы, видимые во всех модулях

export {}; // позволяет сборщику воспринимать этот файл как модуль

declare global {

    interface Product {
        _id: string,
        name: string,
        price: number,
        discount: number,
        wight: string,
        description: string,
        isFavorite: boolean,
        isCart: boolean,
        available: boolean,
        stock: number,
        picture: string
    }

}
