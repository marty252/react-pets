import { useState } from 'react';

function usePagination<T>(data: T[], itemsPerPage: number) {
    // Текущее состояние, по умолчанию = 1
    const [currentPage, setCurrentPage] = useState<number>(1);
    const maxPage = Math.ceil(data.length / itemsPerPage);

    // Публичные функции, которые дергают другие компоненты

    // Отрезает нужное количество карточек для отображения на текущей странице
    function currentData() {
        const start = (currentPage - 1) * itemsPerPage;
        const end = start + itemsPerPage;
        return data.slice(start, end);
    }

    // Клик по стрелкам
    function next() {
        setCurrentPage((currentPage) => Math.min(currentPage + 1, maxPage));
    }

    function prev() {
        setCurrentPage((currentPage) => Math.max(currentPage - 1, 1));
    }

    // Клик по номеру страницы
    // меняет стейт внутри хука
    function setPagePaginate(page: number) {
        const pageNumber = Math.max(1, page);
        setCurrentPage(() => Math.min(pageNumber, maxPage));
    }

    return { next, prev, setPagePaginate, currentData, currentPage, maxPage };
}

export default usePagination;
