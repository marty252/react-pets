import Header from '../header';
import Footer from '../footer';
import { HomePage } from '../../pages/home';
import { productData } from '../../products.js';
import { Box } from '@mui/material';
import * as React from "react";


export const App = () => {
	return (
		<>
			<Header/>
			<Box component='main' sx={{ pt: '30px', pb: '30px' }}>
				<HomePage products={productData}  />
			</Box>
			<Footer />
		</>
	);
};
