import Masonry from '@mui/lab/Masonry';
import ProductCard from '../product-card';
import { Grid, Pagination, Stack, Typography } from '@mui/material';
import { ChangeEvent } from 'react';
import usePagination from '../../hooks/usePagination';

type ProductsListProps = {
    products: Product[];
    type: string;
};

export default function ProductsList({products, type}: ProductsListProps){
    // Пагинация
    const PER_PAGE = 12; // кол-во карточек на одной странице
    const count = Math.ceil(products.length / PER_PAGE);

    const { currentPage, currentData, setPagePaginate } = usePagination<Product>(
        products,
        PER_PAGE
    );
    // Функция перехода на новую страницу
    const handlePageChange = (event: ChangeEvent<unknown>, page: number) => {
        setPagePaginate(page);
    };

    return (
        <>
            {/* Сетка с подгоном размеров карточек*/}
            {type === 'masonry' ? (
                <Masonry columns={{ xs: 1, sm: 2, md: 3, lg: 4 }} spacing={2}>
                    <>
                        {currentData().map((item) => (
                            <ProductCard key={item._id} {...item}  />

                        ))}
                    </>
                </Masonry>
            ) : (
                <Grid container spacing={2}>
                    {currentData().map((item) => (
                        <Grid
                            sx={{ display: 'flex' }}
                            item
                            xs={12}
                            sm={6}
                            md={4}
                            lg={3}
                            key={item._id}>
                            <ProductCard key={item._id} {...item}  />

                        </Grid>
                    ))}
                </Grid>
            )}

            <Stack spacing={2} sx={{ marginTop: 2 }}>
                <Typography>Страница {currentPage}</Typography>
                <Pagination
                    count={count}
                    page={currentPage}
                    onChange={handlePageChange}
                />
            </Stack>
        </>
    );
}