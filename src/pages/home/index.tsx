import { Container, Stack, Switch, Typography } from '@mui/material';
import ProductsList from '../../components/products-list';
import { ChangeEvent, useState } from 'react';

type HomePageProps = {
    products: Product[];
};

export function HomePage({ products }: HomePageProps) {
    // Переключает вид листа ровные плитки или по размеру
    const [isMasonry, setIsMasonry] = useState<boolean>(false);

    function handleSwitchChange(event: ChangeEvent<HTMLInputElement>) {
        setIsMasonry(event.target.checked);
    }

    return (
        <>
            <Container maxWidth='lg'>
                <Stack direction='row' spacing={1} alignItems='center'>
                    <Typography>Grid</Typography>
                    <Switch
                        checked={isMasonry}
                        value='masonry'
                        onChange={handleSwitchChange}
                        name='masonry'
                    />
                    <Typography>Masonry</Typography>
                </Stack>
                <ProductsList
                    type={isMasonry ? 'masonry' : 'grid'}
                    products={products}
                />
            </Container>
        </>
    );
}
